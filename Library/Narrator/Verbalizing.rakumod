# SPDX-License-Identifier: CC0-1.0

unit module Verbalizing;

only sub verbalize(%expresser, Str $expresses) is export {
    my $expression =
        "\e[38:5:" ~ %expresser{"ANSI"} ~ "m"       # ANSI 256-color
        ~ "\e[1m" ~ %expresser{"NAME"} ~ "\e[0m"    # Bold-formatted “NAME”
        ~ "\e[2m: \e[0m"                            # Dim-formatted colon
        ~ "\e[3m" ~ $expresses ~ "\e[0m";           # Italic-formatted text
    say $expression; return $expression;
    # the Return's for test cases in general
};
